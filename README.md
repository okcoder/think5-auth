# think5-auth

#### 项目介绍
这是thinkphp5框架的AUTH权限验证类

#### 安装教程

```
composer require okcoder/think5-auth
```


> src/Main.php文件说明

tp5.1请改为
    ```use think\Db;```
tp5.2请改为（默认）
    ```use think\facade\Db;```

如有类似报错请按上面的修改方式更改

#### 使用说明
1. 是对规则进行认证，不是对节点进行认证。用户可以把节点当作规则名称实现对节点进行认证。
```
$auth = \OkCoder\Auth\Main::instance()->check('规则名称','用户id')

```      
2. 可以同时对多条规则进行认证，并设置多条规则的关系（or或者and）

```
//第三个参数为and时表示，用户需要同时具有规则1和规则2的权限。 当第三个参数为or时，表示用户值需要具备其中一个条件即可。默认为or
$auth = \OkCoder\Auth\Main::instance()->check('规则1,规则2','用户id','and');
```

3. 一个用户可以属于多个用户组(think_auth_group_access表 定义了用户所属用户组)。
    我们需要设置每个用户组拥有哪些规则(think_auth_group 定义了用户组权限)

4. 支持规则表达式。
    在think_auth_rule 表中定义一条规则时，如果type为1， condition字段就可以定义规则表达式。
    如定义{score}>5  and {score}<100  表示用户的分数在5-100之间时这条规则才会通过。


#### 赞助二维码

![](https://gitee.com/uploads/images/2018/0623/112959_9f84f1f7_696921.png "3.png")
![](https://gitee.com/uploads/images/2018/0623/113008_0014aa83_696921.jpeg "4.jpg")