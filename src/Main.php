<?php
/** .-------------------------------------------------------------------
 * | Author: OkCoder <1046512080@qq.com>
 * | Git: https://www.gitee.com/okcoder
 * | Copyright (c) 2012-2019, www.i5920.com. All Rights Reserved.
 * '-------------------------------------------------------------------*/
namespace OkCoder\Auth;

use think\facade\Db;
use think\facade\Request;
use think\facade\Session;

class Main
{
    /**
     * 对象实例
     * @var object
     */
    protected static $instance;

    /**
     * 默认配置
     * @var array
     */
    protected $config = [
        'AUTH_ON' => true,                          // 认证开关
        'AUTH_TYPE' => 1,                           // 认证方式，1为实时认证；2为登录认证。
        'AUTH_GROUP' => 'auth_group',               // 用户组数据表
        'AUTH_GROUP_ACCESS' => 'auth_group_access', // 用户-用户组关系表
        'AUTH_RULE' => 'auth_rule',                 // 权限规则表
        'AUTH_USER' => 'admin'                     // 用户信息表
    ];


    /**
     * 权限认证类
     * 功能特性：
     * 1，是对规则进行认证，不是对节点进行认证。用户可以把节点当作规则名称实现对节点进行认证。
     *      $auth=new Auth();  $auth->check('规则名称','用户id')
     * 2，可以同时对多条规则进行认证，并设置多条规则的关系（or或者and）
     *      $auth=new Auth();  $auth->check('规则1,规则2','用户id','and')
     *      第三个参数为and时表示，用户需要同时具有规则1和规则2的权限。 当第三个参数为or时，表示用户值需要具备其中一个条件即可。默认为or
     * 3，一个用户可以属于多个用户组(think_auth_group_access表 定义了用户所属用户组)。我们需要设置每个用户组拥有哪些规则(think_auth_group 定义了用户组权限)
     *
     * 4，支持规则表达式。
     *      在think_auth_rule 表中定义一条规则时，如果type为1， condition字段就可以定义规则表达式。 如定义{score}>5  and {score}<100  表示用户的分数在5-100之间时这条规则才会通过。
     */
    /**
     * 表结构
     *
     * DROP TABLE IF EXISTS `task_auth_rule`;
     * CREATE TABLE `task_auth_rule` (
     * `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
     * `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
     * `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
     * `type` tinyint(1) NOT NULL DEFAULT 1,
     * `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
     * `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',  # 规则附件条件,满足附加条件的规则,才认为是有效的规则
     * PRIMARY KEY (`id`),
     * UNIQUE KEY `name` (`name`)
     * ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT '规则表';
     * -- ----------------------------
     * DROP TABLE IF EXISTS `task_auth_group`;
     * CREATE TABLE `task_auth_group` (
     * `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
     * `title` char(100) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
     * `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
     * `rules` char(80) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则","隔开',
     * PRIMARY KEY (`id`)
     * ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT '用户组表';
     * -- ----------------------------
     * DROP TABLE IF EXISTS `task_auth_group_access`;
     * CREATE TABLE `task_auth_group_access` (
     * `uid` mediumint(8) unsigned DEFAULT 0 NOT NULL COMMENT '用户id',
     * `group_id` mediumint(8) unsigned DEFAULT 0 NOT NULL COMMENT '用户组id',
     * UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
     * KEY `uid` (`uid`),
     * KEY `group_id` (`group_id`)
     * ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '用户组明细表';
     */

    /**
     * 构造函数
     * Main constructor.
     */
    protected function __construct()
    {
        if ($auth = config('okcoder_auth.')) {
            //可在config目录下新建okcoder_auth.php文件
            $this->config = array_merge($this->config, $auth);
        }
    }

    /**
     * 初始化
     * @param array $options
     * @return object|static
     */
    public static function instance($options = [])
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($options);
        }
        return self::$instance;
    }

    /**
     * 检查权限
     * @param string|array $name 需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param int $uid  认证用户的id
     * @param int $type 认证类型
     * @param string $mode   执行check的模式
     * @param string $relation  如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return bool 通过验证返回true;失败返回false
     */
    public function check($name, $uid = 0, $type = 1, $mode = 'url', $relation = 'or')
    {
        if (!$this->config['AUTH_ON']) return true;
        $authList = $this->getAuthList($uid, $type);
        if (is_string($name)) {
            $name = strtolower($name);
            $name = strpos($name, ',') !== false ? explode(',', $name) : [$name];
            $list = []; //保存验证通过的规则名
            if ('url' == $mode) {
                $REQUEST = unserialize(strtolower(serialize(Request::param())));
            }
            foreach ($authList as $auth) {
                $query = preg_replace('/^.+\?/U', '', $auth);
                if ('url' == $mode && $query != $auth) {
                    parse_str($query, $param);  //解析规则中的param
                    $intersect = array_intersect_assoc($REQUEST, $param);
                    $auth = preg_replace('/\?.*$/U', '', $auth);
                    if (in_array($auth, $name) && $intersect == $param) {
                        //如果节点相符且url参数满足
                        $list[] = $auth;
                    }
                } else {
                    if (in_array($auth, $name)) {
                        $list[] = $auth;
                    }
                }
            }
            if ('or' == $relation && !empty($list)) {
                return true;
            }
            $diff = array_diff($name, $list);
            if ('and' == $relation && empty($diff)) {
                return true;
            }
            return false;
        }
    }

    /**
     * 获得权限列表
     * @param int $uid
     * @param int $type
     * @return array|mixed
     */
    private function getAuthList($uid = 0, $type)
    {
        if (!$uid) return false;
        static $_authList = []; # 保存用户验证通过的权限列表
        $t = implode(',', (array)$type);
        if (isset($_authList[$uid . $t])) {
            return $_authList[$uid . $t];
        }
        if (2 == $this->config['AUTH_TYPE'] && Session::has('_AUTH_LIST_' . $uid . $t)) {
            return Session::get('_AUTH_LIST_' . $uid . $t);
        }

        // 读取用户所属的用户组
        $groups = $this->getGroups($uid);
        $ids = [];  # //保存用户所属用户组设置的所有权限规则id
        foreach ($groups as $group) {
            $ids = array_merge($ids, explode(',', trim($group['rules'], ',')));
        }
        // 去重
        $ids = array_unique($ids);
        if (empty($ids)) {
            $_authList[$uid . $t] = [];
            return [];
        }
        $rules = Db::name($this->config['AUTH_RULE'])
            ->field('condition,name')
            ->where(['type' => $type, 'status' => 1])
            ->whereIn('id', $ids)->select();
        $authList = [];
        foreach ($rules as $rule) {
            if (!empty($rule['condition'])) {
                $user = $this->getUserInfo($uid);
                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $authList[] = strtolower($rule['name']);
                };
            } else {
                //只要存在就记录
                $authList[] = strtolower($rule['name']);
            }
        }
        $_authList[$uid . $t] = $authList;
        if (2 == $this->config['AUTH_TYPE']) {
            //规则列表结果保存到session
            Session::set('_AUTH_LIST_' . $uid . $t, $authList);
        }
        return array_unique($authList);
    }

    /**
     * 根据用户id获取用户组
     * @param int $uid
     * @return array|mixed
     */
    private function getGroups($uid = 0)
    {
        if (!$uid) return false;
        static $groups = [];
        if (isset($groups[$uid])) return $groups[$uid];
        $user_groups = Db::name($this->config['AUTH_GROUP_ACCESS'])
            ->alias('a')
            ->field("a.uid,a.group_id,b.title,b.rules")
            ->leftJoin($this->config['AUTH_GROUP'] . ' b', 'a.group_id=b.id')
            ->where(['a.uid' => $uid, 'b.status' => 1])
            ->select();
        $groups[$uid] = $user_groups ?: [];
        return $groups[$uid];

    }

    /**
     * 获得用户资料
     * @static array $userinfo
     * @param int $uid
     * @return array
     */
    private function getUserInfo($uid = 0)
    {
        static $userinfo = [];
        $user = Db::name($this->config['AUTH_USER']);
        $_pk = is_string($user->getPk()) ? $user->getPk() : 'uid';
        if (!isset($userinfo[$uid])) {
            $userinfo[$uid] = $user->where($_pk, $uid)->find();
        }
        return $userinfo[$uid];
    }


}