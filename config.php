<?php
/** .-------------------------------------------------------------------
 * | Author: OkCoder <1046512080@qq.com>
 * | Git: https://www.gitee.com/okcoder
 * | Copyright (c) 2012-2019, www.i5920.com. All Rights Reserved.
 * '-------------------------------------------------------------------*/

return [
    'AUTH_ON' => true,                          // 认证开关
    'AUTH_TYPE' => 1,                           // 认证方式，1为实时认证；2为登录认证。
    'AUTH_GROUP' => 'auth_group',               // 用户组数据表名
    'AUTH_GROUP_ACCESS' => 'auth_group_access', // 用户-用户组关系表
    'AUTH_RULE' => 'auth_rule',                 // 权限规则表
    'AUTH_USER' => 'admin'                      // 用户信息表
];